package main

import (
	"context"
	"fmt"
	"log"
	"net"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/lequocanh662000/crud-user-with-grpc-and-mysql/ent"
	"gitlab.com/lequocanh662000/crud-user-with-grpc-and-mysql/ent/user"
	pb "gitlab.com/lequocanh662000/crud-user-with-grpc-and-mysql/protos"

	"google.golang.org/grpc"
)

const (
	port = ":50052"
)

var workers []*pb.UserInfo

type workerServer struct {
	pb.UnimplementedWorkerServer
	db *ent.Client
}

// Main
func main() {
	client, err := ent.Open("mysql", "root:P@ssw0rd@tcp(172.30.1.1:13306)/app2?parseTime=True")
	if err != nil {
		log.Fatalf("failed opening connection to mySQL: %v", err)
	}
	defer client.Close()
	// Run the auto migration tool.
	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()

	pb.RegisterWorkerServer(s, &workerServer{pb.UnimplementedWorkerServer{},
		client})

	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

// Create
func (s *workerServer) CreateUser(ctx context.Context, in *pb.CreateRequest) (*pb.CreateReply, error) {
	log.Printf("Received: %v", in)
	res := pb.CreateReply{}
	// work with mySQL
	u, err := s.db.User.
		Create().
		SetAge(int(in.User.GetAge())).
		SetName(in.User.GetName()).
		Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed creating user: %w", err)
	}
	log.Println("user was created: ", u)
	res.Id = int32(u.ID)
	return &res, nil
}

// Update
func (s *workerServer) UpdateUser(ctx context.Context, in *pb.UpdateRequest) (*pb.Status, error) {
	// Error
	if in.GetUser().Name == "" {
		return nil, fmt.Errorf("Name is empty")
	}
	// No Error
	log.Printf("Received: %v", in)
	res := &pb.Status{}
	res.Status = 1
	// work with mySQL
	u1, err1 := s.db.User.Query().Where(user.Name(in.GetUser().GetName())).
		Only(ctx)
	if err1 != nil {
		res.Status = 0
		return res, fmt.Errorf("[1]failed updating user: %w", err1)
	}
	// after query by name
	_, err := s.db.User.Update().Where(user.Name(u1.Name)).
		SetAge(int(in.User.GetAge())).
		Save(ctx)

	if err != nil {
		res.Status = 0
		return res, fmt.Errorf("[2]failed updating user: %w", err)
	}
	return res, nil
}

// Delete
func (s *workerServer) DeleteUser(ctx context.Context, in *pb.DeleteRequest) (*pb.DeleteReply, error) {
	log.Printf("Received: %v", in)
	// Error
	if in.GetId() <= 0 {
		return nil, fmt.Errorf("Error ID")
	}
	// Query UserInfo by ID
	res := &pb.DeleteReply{}
	u1, err1 := s.db.User.Query().Where(user.ID(int(in.Id))).
		Only(ctx)
	if err1 != nil {
		return res, fmt.Errorf("[1]failed updating user: %w", err1)
	}
	// create
	var temp = &pb.UserInfo{}
	temp.Age = int32(u1.Age)
	temp.Name = u1.Name
	res.User = temp
	res.Id = int32(u1.ID)

	// delete in database
	err2 := s.db.User.DeleteOneID(int(in.Id)).Exec(ctx)
	if err2 != nil {
		return nil, fmt.Errorf("[2]failed deleting user: %w", err2)
	}

	return res, nil
}

// Read all Users
func (s *workerServer) ReadUsers(ctx context.Context, in *pb.ReadRequest) (*pb.ReadReply, error) {
	log.Printf("Received: %v", in)
	res := &pb.ReadReply{}
	res.Users = workers
	// work with database

	u, err := s.db.User.Query().All(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed deleting user: %w", err)
	}
	for _, user := range u {
		var temp = &pb.UserInfo{}
		temp.Age = int32(user.Age)
		temp.Name = user.Name
		res.Users = append(res.Users, temp)
	}
	return res, nil
}
