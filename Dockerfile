FROM golang:latest

# Make app folder
RUN mkdir /app
# Set destination for COPY
WORKDIR /app

# Download Go modules
#RUN go get gitlab.com/lequocanh662000/crud-user-with-grpc-and-mysql
COPY . /app/crud-user-with-grpc-and-mysql/
RUN cd /app/crud-user-with-grpc-and-mysql && go mod download all

# Copy the source code. Note the slash at the end, as explained in
# https://docs.docker.com/engine/reference/builder/#copy
#COPY *.go ./

# Build
RUN cd /app/crud-user-with-grpc-and-mysql/server && go build 

# This is for documentation purposes only.
# To actually open the port, runtime parameters
# must be supplied to the docker command.
EXPOSE 50052

# (Optional) environment variable that our dockerised
# application can make use of. The value of environment
# variables can also be set via parameters supplied
# to the docker command on the command line.
#ENV HTTP_PORT=8081

# Run
CMD ["/app/crud-user-with-grpc-and-mysql/server/server"]